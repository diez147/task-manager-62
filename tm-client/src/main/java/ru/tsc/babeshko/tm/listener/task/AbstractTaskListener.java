package ru.tsc.babeshko.tm.listener.task;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.babeshko.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.babeshko.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.babeshko.tm.dto.model.TaskDto;
import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.enumerated.Status;
import ru.tsc.babeshko.tm.listener.AbstractListener;
import ru.tsc.babeshko.tm.util.DateUtil;

import java.util.List;

@Getter
@Component
public abstract class AbstractTaskListener extends AbstractListener {

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private IProjectTaskEndpoint projectTaskEndpoint;

    protected void renderTasks(@Nullable final List<TaskDto> tasks) {
        int index = 1;
        for (TaskDto task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task);
            index++;
        }
    }

    protected void showTask(@NotNull final TaskDto task) {
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(task.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(task.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(task.getDateEnd()));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}