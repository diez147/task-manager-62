package ru.tsc.babeshko.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface ILoggerService {

    void writeLog(@Nullable String message);

}