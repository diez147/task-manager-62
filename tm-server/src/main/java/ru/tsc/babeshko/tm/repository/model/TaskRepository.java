package ru.tsc.babeshko.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.babeshko.tm.model.Task;
import java.util.List;

@Repository
public interface TaskRepository extends AbstractUserOwnedRepository<Task> {

    @NotNull List<Task> findAllByProjectIdAndUserId(@NotNull String projectId, @NotNull String userId);

    @Transactional
    void deleteAllByProjectIdAndUserId(@NotNull String projectId, @NotNull String userId);

}