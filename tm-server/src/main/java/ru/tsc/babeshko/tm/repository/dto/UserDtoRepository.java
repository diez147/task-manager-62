package ru.tsc.babeshko.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.babeshko.tm.dto.model.UserDto;

@Repository
public interface UserDtoRepository extends AbstractDtoRepository<UserDto> {

    @Nullable
    UserDto findFirstByLogin(@NotNull String login);

    @Nullable
    UserDto findFirstByEmail(@NotNull String email);

}